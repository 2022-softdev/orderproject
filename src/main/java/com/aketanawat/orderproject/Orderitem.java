/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.orderproject;

/**
 *
 * @author Acer
 */
public class Orderitem {
    Product product;
    int amount;

    public Orderitem(Product product, int amount) {
        this.product = product;
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmout() {
        return amount;
    }

    public void setAmout(int amount) {
        this.amount = amount;
    }
    
    
}
